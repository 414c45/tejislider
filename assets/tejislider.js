/**
 *  Main scripts
 */
 

document.addEventListener('DOMContentLoaded', function()
{
    var slideshow = document.querySelector('.tejislider');
    var fadein, fadeout; 
    var duration = slideshow.dataset.duration;

    if(slideshow){
        var slideshow_current = 0;
        var slideshow_slides  = document.querySelectorAll('.tejislider__item');
        var slideshow_total   = slideshow_slides.length;
        var current = slideshow_slides[ slideshow_current ];
        current.classList.remove('hidden');
        if(slideshow_total == 1) current.classList.add('inactive');
        var handlers = document.querySelectorAll('[data-go]');
        handlers.forEach( function(handler){
            handler.addEventListener('click', function(){
                current.classList.add('inactive');
                if(fadeout) window.clearInterval(fadeout);
                if(fadein)  window.clearInterval(fadein);
                var opacity = 1;
                fadeout = window.setInterval( function(){
                    opacity -= .15;
                    current.style.opacity = opacity;
                    if( opacity <= 0) {
                        current.style.display = 'none';
                        current.classList.remove('inactive');
                        window.clearInterval(fadeout);
                        slideshow_current = (slideshow_current + (+handler.dataset.go) + slideshow_total) % slideshow_total;
                        current = slideshow_slides[ slideshow_current ];
                        opacity = 0;
                        current.style.opacity = 0;
                        current.style.display = 'block';
                        current.classList.add('inactive');
                        fadein = window.setInterval( function(){
                            opacity += .15;
                            current.style.opacity = opacity;
                            if( opacity >= 1) {
                                window.clearInterval(fadein);
                                current.classList.remove('inactive');
                            }
                        }, 50);
                    }
                }, 50);
            })
        });
        
        if(duration){
            console.log( parseInt(duration)*1000 );
            window.setInterval( function(){
                current.classList.add('inactive');
                    if(fadeout) window.clearInterval(fadeout);
                    if(fadein)  window.clearInterval(fadein);
                    var opacity = 1;
                    fadeout = window.setInterval( function(){
                        opacity -= .15;
                        current.style.opacity = opacity;
                        if( opacity <= 0) {
                            current.style.display = 'none';
                            current.classList.remove('inactive');
                            window.clearInterval(fadeout);
                            slideshow_current = (slideshow_current + 1) % slideshow_total;
                            current = slideshow_slides[ slideshow_current ];
                            opacity = 0;
                            current.style.opacity = 0;
                            current.style.display = 'block';
                            current.classList.add('inactive');
                            fadein = window.setInterval( function(){
                                opacity += .15;
                                current.style.opacity = opacity;
                                if( opacity >= 1) {
                                    window.clearInterval(fadein);
                                    current.classList.remove('inactive');
                                }
                            }, 50);
                        }
                    }, 50);
            }, parseInt(duration)*1000);
        }
    }
});

