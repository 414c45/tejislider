<?php

/**
 * Plugin Name:       Tejislider
 * Description:       Adds a localizable slider, customizable in wp customizer
 * Version:           1.0
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            t/ejido
 * Author URI:        https://tejido.io
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       tejislider
 * Domain Path:       /languages
 */


/**
 *   Adds tejislider settings to WP customizer
 */

add_action( 'customize_register', function($customizer)
{
    // Home slider
  	$customizer->add_section('tejislider_section', [
  			'title'    => 'Tejislider',
  	]);
  	$customizer->add_setting('tejislider_slides', [
          'transport' => 'refresh'
    ]);
    $customizer->add_control('tejislider_slides', [
          'label'    => 'Pasos',
          'description' => 'Número de imágenes en el slider', 
          'type'     => 'text',
          'section'  => 'tejislider_section',
          'priority' => 50,
    ]);  
    $customizer->add_setting('tejislider_autotime', [
          'transport' => 'refresh'
    ]);
    $customizer->add_control('tejislider_autotime', [
          'label'    => 'Duración',
          'description' => 'Duración de la transición en segundos. Déjalo en blanco para que no sea automático',
          'type'     => 'text',
          'section'  => 'tejislider_section',
          'priority' => 50,
    ]);  
    $ns = get_theme_mod('tejislider_slides');
    $langs = [ explode("_", get_locale())[0] ];
    
    if( class_exists('Polylang') ){
        $langs = pll_languages_list();
    };
    for($i = 0; $i < $ns; $i++)
  	{
  			// Add image
  			$name_img     = 'tejislider_image_'.$i;
  			$label_img    = 'Imagen slider ' . ($i+1);
  			$customizer->add_setting($name_img, [
  				'transport' => 'refresh'
  			]);
        $customizer->add_control( new WP_Customize_Image_Control(
             $customizer,
             $name_img,
             [
                'label'    => $label_img,
                'section'  => 'tejislider_section',
                'priority' => 50,
            ]
        ));
  			foreach($langs as $lang)
        {
            $customizer->add_setting(
               'tejislider_text_'.$lang.'_'.$i, 
               [ 'transport' => 'refresh' ]
            );
            $customizer->add_control(
                'tejislider_text_'.$lang.'_'.$i, 
                [
                  'label'    => 'Texto slider '.($i+1).' ['.$lang.']',
                  'type'     => 'text',
                  'section'  => 'tejislider_section',
                  'priority' => 50,
            ]);  
        }
    }	
});

/** 
 *  Adds needed assets
 */

add_action( 'wp_enqueue_scripts', function(){
    wp_enqueue_style(
        'tejislider_css', 
        plugin_dir_url( __FILE__ ).'/assets/tejislider.css', 
        [], 
        '1.0'
    );
    wp_enqueue_script( 
        'tejislider_js', 
        plugin_dir_url( __FILE__ ).'/assets/tejislider.js', 
        [], 
        '1.0', 
        true 
    );
});


/**
 *  Renders a tejislider
 */

function tejislider_render()
{
    $slides = get_theme_mod('tejislider_slides');
    if($slides > 0): ?>
        <div class="tejislider" data-duration="<?= get_theme_mod('tejislider_autotime'); ?>">     
            <?php for($i = 0; $i < $slides; $i++){
                $img  = get_theme_mod('tejislider_image_'. $i);
                $text = get_theme_mod('tejislider_text_' . explode("_", get_locale())[0] . '_'.$i);
                if($img): ?>
                    <figure class="tejislider__item hidden">
                        <div class="left-cover" data-go="-1"></div>
                        <div class="tejislider__image">
                            <img src="<?= $img ?>" />
                        </div>
                        <div class="tejislider__text">
                            <?= $text ?>
                        </div>
                        <div class="right-cover" data-go="1"></div>
                    </figure>
                <?php endif; ?>
            <?php } ?>
        </div>
    <?php endif;
}
